package com.fatunicorn.euler0003;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Artem Chernyshov on 07.05.2017.
 *
 * berechnet die Primfaktoren einer Zahl
 *
 */
public class Euler0003 {

    public static void main(String[] args) {
        if(args.length < 1) {
            System.out.println("Please provide an integer parameter.");
            System.exit(0);
        }

        Long value = new Long(args[0]);
        while(value % 2 == 0) {
            System.out.println(2);
            value = value / 2;
        }

        for(int i = 3; i < Math.sqrt(value); i=i+2) {
            while(value % i == 0) {
                System.out.println(i);
                value = value / i;
            }
        }

        if(value > 2) {
            System.out.println(value);
        }
    }
}
